import pytest
from target_as_sum_of_two_elements import target_as_sum_of_two_elements


def test_2_3():
    assert target_as_sum_of_two_elements([2,-1,3],5) == (2,3)

def test_target_less_than_min():
    assert target_as_sum_of_two_elements([3,4,6],1) == (None,None)

def test_target_superior_than_max():
    assert target_as_sum_of_two_elements([2,5,41,6,9,32],99) == (None,None)

def test_do_not_duplicate_element():
    assert target_as_sum_of_two_elements([2,5,41,6,9,32],4) != (2,2)

def test_when_sum_equals_to_sum_of_duplicated_element():
    assert target_as_sum_of_two_elements([2,6,6,2,4,7],12) == (6,6)