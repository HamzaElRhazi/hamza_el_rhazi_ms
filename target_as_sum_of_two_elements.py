def target_as_sum_of_two_elements(list_integer, target):
    element_1 = list_integer[0]
    list_integer.pop(0)
    if len(list_integer) >= 1:
        element2_to_find = target-element_1
        if element2_to_find in list_integer:
            return (element_1, element2_to_find)
        elif len(list_integer) >= 1:
            return target_as_sum_of_two_elements(list_integer, target)
    else:
        return None, None

print(target_as_sum_of_two_elements([2,6,6,2,4,7],12) )